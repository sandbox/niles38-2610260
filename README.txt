Related Resources for Drupal
============================

//-------------------------------------------------------------------------

Configuration:
1. Download the module.
2. Enable it in admin/modules .
3. You'll now find a Related Resources tab on every node in admin mode.
4. Optionally, you can configure which content types you would like to search in via modifying the related_resources_config.php file. This file contains an array of content types that will not be searched for the page.

//-------------------------------------------------------------------------

Solution to the following issues:
- Finding all the nodes that link to the curent page.
- If you need to delete a page, this module will find the other pages that links to it so you can modify those pages before page deletion. This process prevents broken links on your site.

//-------------------------------------------------------------------------

Other notes:
- Solves the problem discussed here:
  http://drupal.stackexchange.com/questions/22360/finding-all-nodes-not-referenced-by-a-menu-entry-or-another-node
- It's like the SharePoint Show Related Resources tool under Site Management & Content.

//-------------------------------------------------------------------------

Improvement ideas:
- Admin area to manage excluded content types in Drupal 7 settings.
- Detect if the page is used in any menus.
- If the node is an image, find where the image is used.

//-------------------------------------------------------------------------

Thank you!
- Janis M












//-------------------------------------------------------------------------
OTHER 

// regex strings we are searching for... maybe I can figure out how to do this later; it seems EntityFieldQuery doesn't support regex
$regex_search_str = array();
$search_strings[] = "/href=[\\'\"](.*" . str_replace("/", "\/", $long_node_path) . "\\/?)[\\'\"]/";
if ($alt_path) {
	$search_strings[] = "/href=[\\'\"](.*$alt_path\\/?)[\\'\"]/";
}
	
//-------------------------------------------------------------------------

